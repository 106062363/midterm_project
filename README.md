# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : FanFan Chat(Chat App)
* Key functions (add/delete)
    1. 可以聊天
    2. 聊天的歷史記錄
    3. 可以跟新的使用者聊天
    4. 登入/登出
    5. 可顯示Firebase deploy
    6. 資料庫的寫入/讀取
    7. RWD
    8. 第三方登入/登出
    9. CSS動畫
    
* Other functions (add/delete)
    1. 加好友
    2. 換頭像
    3. 改暱稱
    4. 在線狀態
    5. 最後一個留言訊息
    6. 對方的輸入狀態

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|Firebase Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://fanfanchat-af30b.firebaseapp.com
<img src="introduction.JPG" width="1000px" height="600px"></img>
<img src="introduction2.JPG" width="400px" height="750px"></img>
<img src="introduction3.JPG" width="400px" height="750px"></img>

## 有兩個可用的帳號(測試用)
帳號:222@gmail.com  密碼:222222
帳號:123@gmail.com  密碼:123123

# Components Description : 
|功能|描述|
|:-:|:-:|
|1. 可以聊天|a. 多人聊天 b.單對單的朋友聊天|
|2. 聊天的歷史記錄|每個頻道都可以儲存聊天記錄|
|3. 可以跟新的使用者聊天|新的使用者可以立即在多人聊天室聊天|
|4. 登入/登出|a.使用eamil登入 b.有登出鍵|
|5. 可顯示Firebase deploy|可顯示|
|6. 資料庫的寫入/讀取|使用者傳訊息，對方可以立即讀取。|
|7. RWD|a. 會根據螢幕的大小調整字體，不會跑錯版。 b.Tablet以下有移動端的新排版(測試的時候建議F5)|
|8. 第三方登入/登出|Google帳號|
|9. CSS動畫|a.Loading效果 b.選擇朋友的動畫 c.進入聊天室的淡入/淡出動畫(移動端)|


# Other Functions Description(1~10%) : 
|功能|描述|
|:-:|:-:|
|1. 加好友|加好友時輸入對方的暱稱，確定後對方就會彈出加好友的資訊。|
|2. 換頭像|按自己的頭像，就可以選擇照片換美美的頭像，不再是狗狗了。|
|3. 改暱稱|a.按了改暱稱鍵後，可以隨時改自己的暱稱。 b.按了之後不再改可以把滑鼠移出改名框，取消改名的動作。|
|4. 在線狀態|能夠即時知道好友的在線狀態|
|5. 最後一個留言訊息|像FB/Line一樣，進入好友對話前可以知道此頻度最後的對話。|
|6. 對方的輸入狀態|可以知道對方是否正在輸入訊息。|

## Security Report (Optional)
1. 要登入了才可以看到整個Chat的內容。
2. 新創的帳號只有一個叫Global的頻度，要加對方好友，然後他答應後方可聊天。其他人是看不到你們之間的對話。
3. 傳訊息不可輸入html格式的文字，不可只有空白，文字長度不超過100字，以免影響其他使用者使用觀感和資料庫的正常運作。
4. 改暱稱不可有重複，不可有空白和特殊字元，長度少於七個字。
5. 不會儲存和顯示使用者的密碼。
6. Loading效果可防止使用者誤操作，以免導致網站發生錯誤。 
