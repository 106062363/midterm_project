function loadchatdata() { //load chat data
    chatdatame.once('value', (data) => {
        data.forEach((thechat) => {
            if (thechat.child('handle').val() === myemail)
                output.innerHTML += '<div id="messagecon" style="text-align:right;"><span id="mymessage"><strong>' + namelist.get(thechat.val().handle) + ': </strong>' + thechat.val().message + '</span></div>';
            else
                output.innerHTML += '<div id="messagecon"><span id="othermessage"><strong>' + namelist.get(thechat.val().handle) + ': </strong>' + thechat.val().message + '</span></div>'
        });
        loadchat = false;

        if (window.screen.width <= 770 && !firstopen) {
            $("#online-room").fadeOut(200, () => {
                $("#mario-chat").fadeIn(200, () => {
                    chatwindow.scrollTop = chatwindow.scrollHeight;
                });
            });
        }
        chatwindow.scrollTop = chatwindow.scrollHeight;
        firstopen = 0;
    });
}

function loadmember() { //load member
    var myfd = firebase.database().ref(`Users/${myemail}/friend`);
    members_online_con.innerHTML += `<div id="member-online-block">
      <img src="https://firebasestorage.googleapis.com/v0/b/fanfanchat-af30b.appspot.com/o/icon%2Fglobal%2Fdoge2.png?alt=media&token=9c21ceab-1412-404f-96fd-453ab6d98240">
      <div id='info-con-con'>
        <div id='info-con'>
          <span id="member-name">Global</span>
          <span id="status-on">●</span>
        </div>
        <div id="last-msg" class="Global"></div>
      </div>`;
    member_online_block = document.querySelectorAll('#member-online-block');
    member_online_block[0].classList.add('Global');
    member_online_block[0].classList.add('select');
    changemember();

    myfd.on('child_added', (theemail) => { // loadname and status
        var heonline;
        var theiconurl;
        var iconurl = firebase.database().ref(`Users/${theemail.key}/icon`);

        users.once('value', (data) => {
            heonline = data.child(theemail.key).val().online;
        });
        iconurl.once('value', (data) => {
            theiconurl = data.val();
        });

        if (theemail.key != myemail) {
            if (heonline) {
                members_online_con.innerHTML += `<div id="member-online-block">
                <img src="${theiconurl}"><div id='info-con-con'>
                <div id='info-con'><span id="member-name">${namelist.get(theemail.key)}</span><span id="status-on">●</span></div>
                <div id="last-msg" class="${namelist.get(theemail.key)}"></div></div>`;
            } else {
                members_online_con.innerHTML += `<div id="member-online-block">
                <img src="${theiconurl}"><div id='info-con-con'>
                <div id='info-con'><span id="member-name">${namelist.get(theemail.key)}</span><span id="status-off">●</span></div>
                <div id="last-msg" class="${namelist.get(theemail.key)}"></div></div>`;
            }
            //lastmsg = document.querySelectorAll('#last-msg');
            member_online_block = document.querySelectorAll('#member-online-block');
            member_online_block[member_online_block.length - 1].classList.add(theemail.key);
            changemember();
        }
    })
}

function getname() { //get email->name
    users.once('value', (data) => {
        data.forEach((theemail) => {
            namelistforname.set(theemail.val().name, theemail.key);
        });
    });
}



function addname(themail) { //add new user name
    while (1) {
        var special = 0;
        var pattern = ['~', '!', '@', '#', '$', '^', '&', '*', '(', ')', '=', ' ', '|', "'", '"', '{', '}', ':', ';', ',', '<', '>', '/', ' ', '.', '-'];
        var thename = prompt("Please enter your name(No special characters and blank space)", "");
        if (thename) {
            for (var i = 0; i < pattern.length; i++) {
                if (thename.includes(pattern[i])) {
                    special = 1;
                    break;
                }
            }
        } else continue;

        if (!special) {
            if (!namelistforname.has(thename)) {
                users.child(themail).update({
                    name: thename
                });
                updatename.child(themail).set(thename);
                myname_con.innerHTML = `<input id="upload" type="file" accept="image/*"><img src='https://firebasestorage.googleapis.com/v0/b/fanfanchat-af30b.appspot.com/o/icon%2Fglobal%2Fdoge2.png?alt=media&token=9c21ceab-1412-404f-96fd-453ab6d98240' id='myicon'>${thename}`;
                upload = document.getElementById("upload");
                myicon = document.getElementById('myicon');
                uploadbtn();
                alert('Success~~');
                break;
            } else alert('名子已被使用');
        }
    }
}

function nullname() { //check the user name if 0
    var me = firebase.database().ref(`Users/${myemail}/name`);
    me.once('value', (data) => {
        if (data.val() === 0) {
            addname(myemail);
        }
    })
}