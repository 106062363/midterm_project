// Initialize Firebase
var config = {
    apiKey: "AIzaSyDMii4-C293XK5mmM340rXt_u2RRxHkJNw",
    authDomain: "fanfanchat-af30b.firebaseapp.com",
    databaseURL: "https://fanfanchat-af30b.firebaseio.com",
    projectId: "fanfanchat-af30b",
    storageBucket: "fanfanchat-af30b.appspot.com",
    messagingSenderId: "450089757471"
};
firebase.initializeApp(config);

var allcon1 = document.querySelector('#allcon1');
var email = document.querySelector('#email');
var password = document.querySelector('#password');
var login = document.querySelector('#login');
var signin = document.querySelector('#signin');
var googleac = document.querySelector('#googleac');
var body = document.querySelector('body');
var mename = '';

var users = firebase.database().ref('Users');


signin.addEventListener('click', () => {
    firebase.auth().createUserWithEmailAndPassword(email.value, password.value).then(() => {
        $('.loader').fadeIn(100);
        var themail = email.value.replace('@', '_');
        themail = themail.replace('.', '_');

        var theurl;
        var myicon = firebase.storage().ref(`icon/global/doge2.png`);
        myicon.getDownloadURL().then((url) => {
            theurl = url;
        }).then(() => {
            users.child(themail).set({
                name: 0,
                online: 0,
                icon: theurl
            });
        }).then(() => {
            $('.loader').fadeOut();
            alert('註冊成功');
        });

    }).catch((error) => {
        alert(error.message);
    });
});


login.addEventListener('click', () => {
    firebase.auth().signInWithEmailAndPassword(email.value, password.value).then(() => {
        $('.loader').fadeIn(1);
        location.href = "./index.html";


    }).catch((error) => {
        alert(error.message);
    });
});

googleac.addEventListener('click', () => {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then(function (result) {
        $('.loader').fadeIn(100);
        var user = result.user;
        var themail = user.email.replace('@', '_');
        themail = themail.replace('.', '_');

        var theurl;
        var myicon = firebase.storage().ref(`icon/global/doge2.png`);
        myicon.getDownloadURL().then((url) => {
            theurl = url;
        }).then(() => {
            users.once('value', (data) => {
                var prom = new Promise((resolve, reject) => {
                    if (!data.child(themail).exists()) {
                        users.child(themail).set({
                            name: 0,
                            online: 0,
                            icon: theurl
                        });
                    }
                    resolve();
                });

                prom.then(() => {
                    //<div class="loader loader-border is-active"></div>
                    location.href = "./index.html";
                })
            });
        });



    }).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredentialtype that was used.
        var credential = error.credential;
        alert(errorMessage);
    });
});