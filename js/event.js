function confirmbtn() { //add friend confirm button
    confirm_yes = document.querySelectorAll('#confirm-btn-yes');
    confirm_no = document.querySelectorAll('#confirm-btn-no');

    //console.log(confirm_yes.length);

    confirm_yes.forEach(function (thebtn) {
        thebtn.addEventListener('click', () => {
            var removereq = firebase.database().ref(`Users/${myemail}/addfd/${addfriendlist.get(thebtn.className)}`);
            var myfriend = firebase.database().ref(`Users/${myemail}/friend/${thebtn.className}`);
            var yourfriend = firebase.database().ref(`Users/${thebtn.className}/friend/${myemail}`);
            var meindex = firebase.database().ref(`othchatdata/${myemail}/${thebtn.className}`);
            var yourindex = firebase.database().ref(`othchatdata/${thebtn.className}/${myemail}`);

            meindex.set({ // add own database
                chatdata: 1,
                tying: 0
            });
            yourindex.set({ // other member add new database
                chatdata: 1,
                tying: 0
            });

            myfriend.set(1); // add my friend

            yourfriend.set(1); // add your friend

            addfdreq_con.innerHTML = addfdreq_con.innerHTML.replace(`<div id="addfdreq-con"><div style="margin:10px 0px;">${namelist.get(thebtn.className)}</div><div id="confirm-btn-con">
            <button id="confirm-btn-yes" class="${thebtn.className}">Yes</button><button id="confirm-btn-no" class="${thebtn.className}">No</button></div></div>`, '');
            removereq.remove();
            confirmbtn();
        });
    });

    confirm_no.forEach(function (thebtn) {
        thebtn.addEventListener('click', () => {
            var removereq = firebase.database().ref(`Users/${myemail}/addfd/${addfriendlist.get(thebtn.className)}`);

            addfdreq_con.innerHTML = addfdreq_con.innerHTML.replace(`<div id="addfdreq-con"><div style="margin:10px 0px;">${namelist.get(thebtn.className)}</div><div id="confirm-btn-con">
            <button id="confirm-btn-yes" class="${thebtn.className}">Yes</button><button id="confirm-btn-no" class="${thebtn.className}">No</button></div></div>`, '');
            removereq.remove();
            confirmbtn();
        });
    });
}

function addfriendbtn() { // add friend button
    addfd.addEventListener('click', function () {
        var thename = prompt("Who?", "");
        var myfd = firebase.database().ref(`Users/${myemail}/friend`);
        var hisfdreq = firebase.database().ref(`Users/${namelistforname.get(thename)}/addfd`);

        var existfriend = 0;
        var existreq = 0;
        myfd.once('value', (data) => {
            existfriend = data.child(namelistforname.get(thename)).exists();
            //console.log(data.child(namelistforname.get(thename)).exists());
        });

        hisfdreq.once('value',(data)=>{
            data.forEach(thereq=>{
                existreq = thereq.val()==myemail;
            });
            
        })

        if (!existfriend && !existreq) {
            if (namelistforname.has(thename) && namelistforname.get(thename) != myemail) {
                var req = firebase.database().ref(`Users/${namelistforname.get(thename)}/addfd`);
                req.push(myemail);
            } else {
                if (namelistforname.get(thename) == myemail)
                    alert('沒有朋友不要加自己^^');
                else
                    alert('無效用戶');
            }
        } else alert('已在朋友名單或發出邀請');
    });
}


function changebtn() { // send msg btn
    btn.addEventListener('click', function () {
        sendmsg();
        if (!allchannel) metying.set(0);
        else alltying.child(myemail).set(0);
    });
}

function changenamebtn() { //change name button
    changename.addEventListener('click', function () {
        var myname = myname_con.textContent;

        myname_con.innerHTML = `<input id="chmyname" type="text" placeholder="New name" style="font-size:1.3rem; width:90%;"/>`;
        var chmyname = document.querySelector('#chmyname');
        chmyname.addEventListener('keyup', event => {
            var pattern = ['~', '!', '@', '#', '$', '^', '&', '*', '(', ')', '=', ' ', '|', "'", '"', '{', '}', ':', ';', ',', '<', '>', '/', '　', '.', '-'];
            var special = 0;
            if (event.keyCode == '13') {
                if (chmyname.value) {
                    for (var i = 0; i < pattern.length; i++) {
                        // console.log('ok')
                        if (chmyname.value.includes(pattern[i])) {
                            special = 1;
                            break;
                        }
                    }
                } else alert('不能空白');

                if (special) alert('不可有特殊符號或空白');
                else {
                    if(chmyname.value.length<=7){
                        if (!namelistforname.get(chmyname.value)) {
                            //console.log(namelist.get(chmyname.value))
                            var theurl;
                            var chmynameref = firebase.database().ref(`Users/${myemail}/name`);
                            var myicon = firebase.storage().ref(`icon/${myemail}/icon`);
                            myicon.getDownloadURL().then((url) => {
                                theurl = url;
                            }).then(() => {
                                chmynameref.set(chmyname.value);
                                myname_con.innerHTML = `<input id="upload" type="file" accept="image/*"><img src='${theurl}' id='myicon'>${chmyname.value}`;
                                upload = document.getElementById("upload");
                                myicon = document.getElementById('myicon');
                                namelist.set(myemail, chmyname.value);
                                namelistforname.set(chmyname.value, myemail);
                                
                            }).then(()=>$('.loader').fadeOut(100))
                            .catch((e) => {
                                if (e.code == 'storage/object-not-found') {
                                    chmynameref.set(chmyname.value);
                                    myname_con.innerHTML = `<input id="upload" type="file" accept="image/*"><img src='https://firebasestorage.googleapis.com/v0/b/fanfanchat-af30b.appspot.com/o/icon%2Fglobal%2Fdoge2.png?alt=media&token=9c21ceab-1412-404f-96fd-453ab6d98240' id='myicon'>${chmyname.value}`;
                                    upload = document.getElementById("upload");
                                    myicon = document.getElementById('myicon');
                                    namelist.set(myemail, chmyname.value);
                                    namelistforname.set(chmyname.value, myemail);
                                   // $('.loader').fadeOut(100);
                                }
                            });
    
                        } else alert('名字已經使用');
                    }
                    else  alert('名字長度要少於7')
                }
            }
        });

        chmyname.addEventListener('mouseout', () => {
            $('.loader').fadeIn(100);
            var theurl;
            var myicon = firebase.storage().ref(`icon/${myemail}/icon`);
            myicon.getDownloadURL().then((url) => {
                theurl = url;
            }).then(() => {
                myname_con.innerHTML = `<input id="upload" type="file" accept="image/*"><img src='${theurl}' id='myicon'>${myname}`;
                upload = document.getElementById("upload");
                myicon = document.getElementById('myicon');
                $('.loader').fadeOut(100);
            }).catch((e) => {
                if (e.code == 'storage/object-not-found') {
                    myname_con.innerHTML = `<input id="upload" type="file" accept="image/*"><img src='https://firebasestorage.googleapis.com/v0/b/fanfanchat-af30b.appspot.com/o/icon%2Fglobal%2Fdoge2.png?alt=media&token=9c21ceab-1412-404f-96fd-453ab6d98240' id='myicon'>${myname}`;
                    upload = document.getElementById("upload");
                    myicon = document.getElementById('myicon');
                }
                $('.loader').fadeOut(100);
            }); 
        });
    });
}


function sendmsg() { // check message input
    var pattern = ['<','>'];
    var cansend=1;
    if (message.value) {
        if(message.value.length<=100){
            if(message.value[0] != ' ' && message.value[0] != '　'){
                for(let thepa of pattern){
                    if (message.value.includes(thepa)){
                        cansend = 0;
                        break;
                    }
                }
            } 
            else cansend = 0;
    
            if(cansend){
                chatdatame.push({
                    message: message.value,
                    handle: myemail
                });
    
                if (!allchannel) {
                    chatdatayou.push({
                        message: message.value,
                        handle: myemail
                    });
                }
            }
            else alert('禁止空白');
        }
        else alert('不要寫論文');
        message.value = '';
        cantyping = true;
    }
}

function changemessage() { // check message input and tying event
    message.addEventListener('keyup', function (event) { // keyboard event
        if (event.keyCode == '13') {
            sendmsg();
            if (!allchannel) metying.set(0);
            else alltying.child(myemail).set(0);

        } else if (message.value && cantyping) {
            if (!allchannel) metying.set(1);
            else alltying.child(myemail).set(1);
            cantyping = false;

        } else if (!message.value) {
            if (!allchannel) metying.set(0);
            else alltying.child(myemail).set(0);
            cantyping = true;

        }
    });
}

function changemember() { // new member event after add friend
    lastmsg = document.querySelectorAll('#last-msg');
    member_online_block = document.querySelectorAll('#member-online-block');
    //console.log(member_online_block)
    member_online_block.forEach(function (themember, index) { // member block evnet
        themember.addEventListener('click', function () {
            member_online_block[lastselect].classList.remove('select');

            alltying.child(myemail).set(0); //no tying global->other
            metying.set(0); //no tying other->global

            cantyping = true;
            message.value = '';
            youtying.off('value');
            chatdatame.off('child_added');
            lastmsgall.off('child_added');
            lastmsgoth.off('child_added');

            if (themember.className == 'Global') {
                chatdatame = firebase.database().ref('allchatdata'); // it can change other chat data
                chatdatayou = firebase.database().ref('zdefault'); // default
                youtying = firebase.database().ref('zdefault'); // default
                metying = firebase.database().ref('alltying/' + myemail);
                allchannel = true;
            } else {
                chatdatame = firebase.database().ref('othchatdata/' + myemail + '/' + themember.className + '/chatdata');
                chatdatayou = firebase.database().ref('othchatdata/' + themember.className + '/' + myemail + '/chatdata');
                youtying = firebase.database().ref('othchatdata/' + myemail + '/' + themember.className + '/tying');
                metying = firebase.database().ref('othchatdata/' + themember.className + '/' + myemail + '/tying');
                allchannel = false;
            }


            metying.onDisconnect().set(0);
            feedback.innerHTML = '';
            output.innerHTML = '';
            loadchat = true;

            themember.classList.add('select');
            lastselect = index;


            message.removeEventListener('keyup', changemessage);
            btn.removeEventListener('click', changebtn);

            listenlastmsgall();
            listenlastmsgoth();
            loadchatdata();
            changechatdatame();

            youtying.on('value', (data) => {
                if (!allchannel) {
                    if (data.val())
                        feedback.innerHTML += '<div id="messagecon" style="text-align:center;"><span id="tyingmessage"><strong>' + namelist.get(themember.className.replace(' select', '')) + ' </strong>...</span></div>';
                    else
                        feedback.innerHTML = feedback.innerHTML.replace('<div id="messagecon" style="text-align:center;"><span id="tyingmessage"><strong>' + namelist.get(themember.className.replace(' select', '')) + ' </strong>...</span></div>', '');

                    $("#chat-window").animate({
                        "scrollTop": chatwindow.scrollHeight
                    }, 200);
                }
            });
        });
    });
}

function loginoutbtn() {
    login_out_in_btn.addEventListener('click', function () {

        if (this.className == 'logout') {
            me.update({
                online: 0
            });
            firebase.auth().signOut().then(() => {
                window.location.reload();
            });
        } else {
            location.href = "./login.html";
        }
    });
}
loginoutbtn();

function uploadbtn() {
    upload.addEventListener("change", function () {
        $('.loader').fadeIn(100);
        var file = this.files[0];
        var theimg = firebase.storage().ref(`icon/${myemail}/icon`);

        theimg.put(file).then(() => {
                theimg.getDownloadURL().then(function (theurl) {
                    var changeicon = firebase.database().ref(`Users/${myemail}/icon`);
                    changeicon.set(theurl);
                    myicon.src = theurl;
                    $('.loader').fadeOut(100);
                    //console.log('ok');
                });
        })
        .catch(e => {
            if (e.code == 'storage/object-not-found') {
                var theimg = firebase.storage().ref(`icon/${myemail}/icon`);
                theimg.put(file).then(() => {
                    theimg.getDownloadURL().then(function (theurl) {
                        var changeicon = firebase.database().ref(`Users/${myemail}/icon`);
                        changeicon.set(theurl);
                        myicon.src = theurl;
                        $('.loader').fadeOut(100);
                        //console.log('ok');
                    });
                });
            }
        });
    });
}


function backmemberbtn(){
    backmember.addEventListener('click',function(){
        $("#mario-chat").fadeOut(200,()=>{
            $("#online-room").fadeIn(200);
        });
    });
}