function allty() { //listen tying
    alltying.on('child_changed', (data) => {
        if (allchannel) {
            var theemail = data.key;
            if (theemail != myemail) {
                if (data.val())
                    feedback.innerHTML += '<div id="messagecon" style="text-align:center;"><span id="tyingmessage"><strong>' + namelist.get(theemail) + ' </strong>...</span></div>';
                else
                    feedback.innerHTML = feedback.innerHTML.replace('<div id="messagecon" style="text-align:center;"><span id="tyingmessage"><strong>' + namelist.get(theemail) + ' </strong>...</span></div>', '');
                $("#chat-window").animate({
                    "scrollTop": chatwindow.scrollHeight
                }, 200);
            }
        }
    });
}

function changechatdatame() { //listen chatdata
    chatdatame.on('child_added', (data) => {
        if (!loadchat) {
            if (data.val().handle === myemail)
                output.innerHTML += '<div id="messagecon" style="text-align:right;"><span id="mymessage"><strong>' + namelist.get(data.val().handle) + ': </strong>' + data.val().message + '</span></div>';
            else
                output.innerHTML += '<div id="messagecon"><span id="othermessage"><strong>' + namelist.get(data.val().handle) + ': </strong>' + data.val().message + '</span></div>'

            $("#chat-window").animate({
                "scrollTop": chatwindow.scrollHeight
            }, 200);
        }
    });
}



function addfriendsreq() { //listen add friend req
    addfreinds.on('child_added', (data) => {
        addfdreq_con.innerHTML += `<div id='addfdreq-con'><div style="margin:10px 0px;">${namelist.get(data.val())}</div><div id='confirm-btn-con'>
            <button id='confirm-btn-yes' class='${data.val()}'>Yes</button><button id='confirm-btn-no' class='${data.val()}'>No</button></div></div>`;
        addfriendlist.set(data.val(), data.key);
        confirmbtn();
    });
}

function updatenamelist() { //listen new user name
    updatename.on('child_added', (data) => {
        namelist.set(data.key, data.val());
        namelistforname.set(data.val(), data.key);
        updatename.set(0);
    });
}

users.on('child_changed', (data) => { //listen online status
    if (data.val().online) {
        members_online_con.innerHTML = members_online_con.innerHTML.replace(namelist.get(data.key) + '</span><span id="status-off">●', namelist.get(data.key) + '</span><span id="status-on">●');
    } else {
        members_online_con.innerHTML = members_online_con.innerHTML.replace(namelist.get(data.key) + '</span><span id="status-on">●', namelist.get(data.key) + '</span><span id="status-off">●');
    }
    changemember(); //update all key
});

function listenlastmsgall() {
    //console.log('all')
    var init = false;

    lastmsgall.on('child_added', (data) => {
        if (init) {
            try{
                lastmsg[0].textContent = `${namelist.get(data.val().handle)}: ${data.val().message}`;
            }
            catch(e){
                var lastmsg = document.querySelector('#last-msg');
                lastmsg.textContent = `${namelist.get(data.val().handle)}: ${data.val().message}`;
            }
        }
    });

    lastmsgall.once('value', () => { //get last chatdata
        init = true;
        var thelast = lastmsgall.limitToLast(1);
        thelast.once('value', data => {
            try{
                data.forEach(dd => {
                    lastmsg[0].textContent = `${namelist.get(dd.val().handle)}: ${dd.val().message}`;
                });
            }
            catch(e){
                var lastmsg = document.querySelector('#last-msg');
                data.forEach(dd => {
                    lastmsg.textContent = `${namelist.get(dd.val().handle)}: ${dd.val().message}`;
                });
            }

        });
    });

}

function listenlastmsgoth() {
    //console.log('oth')
    var init = false;
    lastmsgoth.on('child_changed', (data) => {
        if (init) {
            //console.log(data.key)
            var thelast = firebase.database().ref(`othchatdata/${myemail}/${data.key}/chatdata`).limitToLast(1);
            thelast.once('value', theuni => {
                theuni.forEach(dd => {
                    lastmsg.forEach((themsg) => {
                        //console.log(themsg.className,namelist.get(data.key))

                        if (themsg.className == namelist.get(data.key)) {
                            themsg.textContent = `${namelist.get(dd.val().handle)}: ${dd.val().message}`;
                        }
                    })
                });
            });
        }
    });

    lastmsgoth.once('value', (data) => { //get last chatdata
        data.forEach((email) => {
            var thelast = firebase.database().ref(`othchatdata/${myemail}/${email.key}/chatdata`).limitToLast(1);
            thelast.once('value', theuni => {
                theuni.forEach(dd => {
                    //console.log(dd.val().handle, dd.val().message);
                    lastmsg.forEach((themsg) => {
                        //console.log(themsg)
                        if (themsg.className == namelist.get(email.key)) {
                            themsg.textContent = `${namelist.get(dd.val().handle)}: ${dd.val().message}`;
                        }
                    })
                });
            });
            init = true;
        });
    });
}